/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{ts,tsx}'],
	theme: {
		extend: {
			colors: {
				primary: {
					DEFAULT: '#5DB075',
					100: '#D7F4E4',
					200: '#B1E9CE',
					300: '#8BDEC0',
					400: '#64D3B1',
					500: '#5DB075',
					600: '#4F9860',
					700: '#3E794B',
					800: '#2E5936',
					900: '#1F3A21'
				}
			}
		}
	},
	plugins: []
};
