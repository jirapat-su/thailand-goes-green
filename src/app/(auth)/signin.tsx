import Button from '@/components/Button';
import Input from '@/components/Input';
import { AppDispatch } from '@/store';
import { setAuthData } from '@/store/slices/authSlice';
import { useRouter } from 'expo-router';
import { useCallback } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Keyboard, KeyboardAvoidingView, Platform, Text, TouchableWithoutFeedback, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useDispatch } from 'react-redux';

interface LoginFormProps {
	email: string;
	password: string;
}

export default function SignInPage() {
	const dispatch = useDispatch<AppDispatch>();
	const router = useRouter();

	const {
		control,
		handleSubmit,
		formState: { errors }
	} = useForm<LoginFormProps>({
		defaultValues: {
			email: '',
			password: ''
		}
	});

	const onSubmit = useCallback(
		(data: LoginFormProps) => {
			dispatch(setAuthData({ username: data.email, token: Math.random().toString(36).substring(2) }));
			router.replace('/profile');
		},
		[dispatch, router]
	);

	return (
		<KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} className="flex-1">
			<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
				<SafeAreaView className="flex-1 justify-around bg-white px-4">
					<View className="flex gap-y-4">
						<Text className="mb-4 text-center text-4xl">Log In</Text>

						<View className="block">
							<Controller
								control={control}
								rules={{
									required: 'Email is required.',
									pattern: {
										value: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
										message: 'Invalid email format.'
									}
								}}
								name="email"
								render={({ field: { onChange, onBlur, value } }) => (
									<Input placeholder="Email" onBlur={onBlur} onChangeText={onChange} value={value} />
								)}
							/>
							{errors.email && <Text className="text-red-600">{errors.email.message}</Text>}
						</View>

						<View className="block">
							<Controller
								control={control}
								rules={{
									required: 'Password is required.'
								}}
								name="password"
								render={({ field: { onChange, onBlur, value } }) => (
									<Input secureTextEntry placeholder="Password" onBlur={onBlur} onChangeText={onChange} value={value} />
								)}
							/>
							{errors.password && <Text className="text-red-600">{errors.password.message}</Text>}
						</View>
					</View>
					<View className="flex items-center gap-4">
						<Button primary text="Log In" onPress={handleSubmit(onSubmit)} />
						<Text className="text-base text-primary">Forgot your password?</Text>
					</View>
				</SafeAreaView>
			</TouchableWithoutFeedback>
		</KeyboardAvoidingView>
	);
}
