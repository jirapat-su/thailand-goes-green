import Button from '@/components/Button';
import { useRouter } from 'expo-router';
import { Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function FirstPage() {
	const router = useRouter();

	return (
		<SafeAreaView className="flex-1 justify-around bg-primary">
			<Text className="text-center text-5xl uppercase text-white">{`thailand\ngoes\ngreen`}</Text>
			<Text className="ml-12 text-left text-4xl uppercase text-white">{`better\nfresh air,\nbetter where\nwe live`}</Text>
			<View className="w-full pl-8 pr-8">
				<Button
					text="Log In"
					onPress={() => {
						router.push('/signin');
					}}
				/>
			</View>
		</SafeAreaView>
	);
}
