import { blurhash } from '@/components/Avatar';
import Button from '@/components/Button';
import Input from '@/components/Input';
import { AppDispatch, RootState } from '@/store';
import { setPlantScore } from '@/store/slices/plantSlice';
import DateTimePicker, { DateTimePickerEvent } from '@react-native-community/datetimepicker';
import { Image } from 'expo-image';
import { useRouter } from 'expo-router';
import React, { useCallback, useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Keyboard, KeyboardAvoidingView, Platform, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useDispatch, useSelector } from 'react-redux';

interface PlantFormProps {
	description: string;
	date: string;
	width: string;
	height: string;
	district: string;
	province: string;
}

export default function PlantPage() {
	const dispatch = useDispatch<AppDispatch>();
	const myPlantScore = useSelector((state: RootState) => state.plant.score);
	const [timeSelected, setTimeSelected] = useState<Date>();
	const [plantSelected, setPlantSelected] = useState<'pot' | 'ground' | undefined>(undefined);
	const router = useRouter();
	const [showCalendar, setShowCalendar] = useState(false);
	const {
		control,
		handleSubmit,
		setValue,
		formState: { errors }
	} = useForm<PlantFormProps>({
		defaultValues: {
			description: '',
			date: '',
			district: '',
			province: ''
		}
	});

	useEffect(() => {
		if (timeSelected) setValue('date', timeSelected.toLocaleDateString());
	}, [setValue, timeSelected]);

	const handleBackPage = useCallback(() => {
		if (router.canGoBack()) {
			router.back();
			return;
		}

		router.replace('/profile');
	}, [router]);

	const handleChangeDate = useCallback(
		(event: DateTimePickerEvent | null, date?: Date | undefined) => {
			if (date) {
				setTimeSelected(date);
				setValue('date', date.toLocaleDateString());
			}
		},
		[setValue, setTimeSelected]
	);

	const onSubmit = useCallback(
		(data: PlantFormProps) => {
			dispatch(setPlantScore(myPlantScore + 1));
			router.push(`/thanks?district=${data.district}&province=${data.province}`);
		},
		[dispatch, myPlantScore, router]
	);

	return (
		<KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} className="flex-1">
			<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
				<SafeAreaView className="flex-1 bg-white">
					<React.Fragment>
						<View className="relative mb-8">
							<Text className="absolute left-0 top-[32%] z-40 p-4 text-center text-xl text-primary" onPress={handleBackPage}>
								Back
							</Text>
							<Text className="mt-8 text-center text-5xl">Your Plant</Text>
						</View>
						<View className="mt-8 gap-y-4 px-4">
							<View className="block">
								<Controller
									control={control}
									rules={{
										required: 'This field is required.'
									}}
									name="description"
									render={({ field: { onChange, onBlur, value } }) => (
										<Input placeholder="What's your plant" onBlur={onBlur} onChangeText={onChange} value={value} />
									)}
								/>
								{errors.description && <Text className="text-red-600">{errors.description.message}</Text>}
							</View>

							<View className="block">
								<Controller
									control={control}
									rules={{
										required: 'This field is required.'
									}}
									name="date"
									render={({ field: { onChange, onBlur, value } }) => (
										<TouchableOpacity onPress={() => setShowCalendar(true)}>
											<Input
												editable={false}
												pointerEvents="none"
												placeholder="Plant date"
												onBlur={onBlur}
												onChangeText={onChange}
												value={value}
											/>
										</TouchableOpacity>
									)}
								/>
								{errors.date && <Text className="text-red-600">{errors.date.message}</Text>}

								{(() => {
									if (!showCalendar) return null;

									switch (Platform.OS) {
										case 'ios':
											return (
												<View className="my-4 flex items-center">
													<DateTimePicker mode="date" display="calendar" value={timeSelected || new Date()} onChange={handleChangeDate} />
													<Button
														text="OK"
														onPress={() => {
															setShowCalendar(false);
															if (!timeSelected) handleChangeDate(null, new Date());
														}}
														className="mt-4 w-[150px]"
													/>
												</View>
											);
										case 'android':
											return (
												<DateTimePicker
													mode="date"
													display="inline"
													value={new Date()}
													onChange={(e) => (handleChangeDate(e), setShowCalendar(false))}
												/>
											);
										default:
											return null;
									}
								})()}
							</View>

							<View className="flex w-full flex-row">
								<View className="mr-2 flex-1">
									<Controller
										control={control}
										rules={{
											required: 'This field is required.'
										}}
										name="width"
										render={({ field: { onChange, onBlur, value } }) => (
											<Input placeholder="Plant width" onBlur={onBlur} onChangeText={onChange} value={value} keyboardType="number-pad" />
										)}
									/>
									{errors.width && <Text className="text-red-600">{errors.width.message}</Text>}
								</View>

								<View className="ml-2 flex-1">
									<Controller
										control={control}
										rules={{
											required: 'This field is required.'
										}}
										name="height"
										render={({ field: { onChange, onBlur, value } }) => (
											<Input placeholder="Plant height" onBlur={onBlur} onChangeText={onChange} value={value} keyboardType="number-pad" />
										)}
									/>
									{errors.height && <Text className="text-red-600">{errors.height.message}</Text>}
								</View>
							</View>

							<View className="block">
								<Controller
									control={control}
									rules={{
										required: 'This field is required.'
									}}
									name="district"
									render={({ field: { onChange, onBlur, value } }) => (
										<Input placeholder="District" onBlur={onBlur} onChangeText={onChange} value={value} />
									)}
								/>
								{errors.district && <Text className="text-red-600">{errors.district.message}</Text>}
							</View>

							<View className="block">
								<Controller
									control={control}
									rules={{
										required: 'This field is required.'
									}}
									name="province"
									render={({ field: { onChange, onBlur, value } }) => (
										<Input placeholder="Province" onBlur={onBlur} onChangeText={onChange} value={value} />
									)}
								/>
								{errors.province && <Text className="text-red-600">{errors.province.message}</Text>}
							</View>

							<View>
								<Text className="mb-4 text-xl">Plant Type</Text>

								<View className="flex w-full flex-row justify-around">
									<TouchableOpacity onPress={() => setPlantSelected('pot')}>
										<View
											className={`bg-lightgray h-32 w-32 overflow-hidden rounded-2xl border-8 border-primary ${
												plantSelected?.indexOf('pot') !== -1 ? 'bg-primary-100' : 'bg-white'
											}`}
										>
											<Image
												source={'https://cdn-icons-png.flaticon.com/512/628/628324.png'}
												placeholder={blurhash}
												contentFit="cover"
												transition={1000}
												className="h-full w-full flex-1"
											/>
										</View>
									</TouchableOpacity>

									<TouchableOpacity onPress={() => setPlantSelected('ground')}>
										<View
											className={`bg-lightgray h-32 w-32 overflow-hidden rounded-2xl border-8 border-primary ${
												plantSelected?.indexOf('ground') !== -1 ? 'bg-primary-100' : 'bg-white'
											}`}
										>
											<Image
												source={'https://cdn-icons-png.flaticon.com/512/1892/1892747.png'}
												placeholder={blurhash}
												contentFit="cover"
												transition={1000}
												className="h-full w-full flex-1"
											/>
										</View>
									</TouchableOpacity>
								</View>

								{!plantSelected && <Text className="text-red-600">Please select a plant type.</Text>}
							</View>

							<Button primary text="Register" onPress={handleSubmit(onSubmit)} />
						</View>
					</React.Fragment>
				</SafeAreaView>
			</TouchableWithoutFeedback>
		</KeyboardAvoidingView>
	);
}
