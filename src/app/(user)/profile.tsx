import Avatar from '@/components/Avatar';
import Button from '@/components/Button';
import CircularProgressBar from '@/components/CircularProgressBar';
import useAxios from '@/hooks/useAxios';
import { AppDispatch, RootState } from '@/store';
import { setAuthData } from '@/store/slices/authSlice';
import { setPlantScore } from '@/store/slices/plantSlice';
import { useRouter } from 'expo-router';
import { useCallback, useEffect } from 'react';
import { Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useDispatch, useSelector } from 'react-redux';

export default function ProfilePage() {
	const router = useRouter();
	const dispatch = useDispatch<AppDispatch>();
	const myPlantScore = useSelector((state: RootState) => state.plant.score);
	const getPlantScore = useAxios<number>('https://www.random.org/integers/?num=1&min=100&max=1000&col=1&base=10&format=plain&rnd=new', {
		method: 'GET'
	});

	useEffect(() => {
		if (myPlantScore === 0) getPlantScore.fetch();
	}, [myPlantScore]);

	useEffect(() => {
		if (getPlantScore.data) dispatch(setPlantScore(getPlantScore.data));
	}, [getPlantScore.data]);

	const handleLogout = useCallback(() => {
		dispatch(setAuthData(null));
		dispatch(setPlantScore(0));
	}, [dispatch]);

	const handleRegisterPlant = useCallback(() => {
		router.push('/plant');
	}, [router]);

	return (
		<View className="relative flex-1 bg-white">
			<View className="h-[35%] w-screen bg-primary"></View>
			<SafeAreaView className="absolute left-0 top-0  w-screen flex-1 gap-y-[12%] bg-transparent">
				<View className="relative">
					<Text className="mt-8 text-center text-5xl text-white">Profile</Text>
					<Text className="absolute right-0 top-[32%] z-40 p-4 text-center text-xl text-white" onPress={handleLogout}>
						Logout
					</Text>
				</View>

				<View className="flex items-center">
					<View className="mb-4">
						<Avatar className="mb-4" url="https://i.ytimg.com/vi/QIFnEMNFrxc/maxresdefault.jpg" />
					</View>
					<Text className="text-center text-4xl font-semibold uppercase">Boss Baby</Text>
					<Text className="text-center text-2xl font-semibold">LIVE at Bang Na, Bangkok</Text>
				</View>

				<View className="flex content-center items-center">
					<CircularProgressBar score={myPlantScore} />
				</View>

				<View className="px-4">
					<Button primary text="Register Your Plant" onPress={() => handleRegisterPlant()} />
				</View>
			</SafeAreaView>
		</View>
	);
}
