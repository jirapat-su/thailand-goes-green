import { Stack } from 'expo-router';

export default function UserLayout() {
	return (
		<Stack>
			<Stack.Screen name="profile" options={{ headerShown: false, animation: 'slide_from_bottom' }} />
			<Stack.Screen name="plant" options={{ headerShown: false, animation: 'slide_from_bottom' }} />
			<Stack.Screen name="thanks" options={{ headerShown: false, animation: 'slide_from_bottom' }} />
		</Stack>
	);
}
