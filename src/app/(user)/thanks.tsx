import Button from '@/components/Button';
import { RootState } from '@/store';
import { useGlobalSearchParams, useRouter } from 'expo-router';
import { useCallback } from 'react';
import { Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useSelector } from 'react-redux';

export default function ThanksPage() {
	const router = useRouter();
	const myPlantScore = useSelector((state: RootState) => state.plant.score);
	const { district = 'hello', province = 'world!' } = useGlobalSearchParams<{ district: string; province: string }>();

	const backToPlant = useCallback(() => {
		if (router.canGoBack()) {
			router.back();
			return;
		}

		router.push('/plant');
	}, [router]);

	const backToProfile = useCallback(() => {
		router.push('/profile');
	}, [router]);

	return (
		<SafeAreaView className="flex-1 justify-around bg-primary px-4">
			<View className="rounded-lg bg-white px-4 py-8 shadow-md">
				<Text className="mb-8 text-center text-3xl font-semibold">Thank You 🌱</Text>
				<Text className="mb-4 text-lg">
					You just make {district}, {province} more fresh air !!
				</Text>
				<Text className="mb-8 text-lg">However, there are still {Math.abs(myPlantScore - 1000)} plants to do more.</Text>

				<Button primary text="Register Your Plant" className="mb-4" onPress={backToPlant} />
				<Button text="Profile" onPress={backToProfile} />
			</View>
		</SafeAreaView>
	);
}
