import '@/global.css';
import { RootState, persistor, store } from '@/store';
import FontAwesome from '@expo/vector-icons/FontAwesome';
import { DefaultTheme, ThemeProvider } from '@react-navigation/native';
import { useFonts } from 'expo-font';
import { SplashScreen, Stack, useRouter } from 'expo-router';
import { useEffect } from 'react';
import { Text } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider, useSelector } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

export { ErrorBoundary } from 'expo-router';

SplashScreen.preventAutoHideAsync();

export const unstable_settings = {
	// Ensure that reloading on `/modal` keeps a back button present.
	initialRouteName: '/'
};

export default function RootLayout() {
	const [loaded, error] = useFonts({
		SpaceMono: require('@/assets/fonts/SpaceMono-Regular.ttf'),
		...FontAwesome.font
	});

	useEffect(() => {
		if (error) throw error;
	}, [error]);

	if (!loaded) return null;

	return (
		<Provider store={store}>
			<PersistGate loading={<Text>Loading...</Text>} persistor={persistor}>
				<App />
			</PersistGate>
		</Provider>
	);
}

function App() {
	const authData = useSelector((state: RootState) => state.auth.data);
	const router = useRouter();

	useEffect(() => {
		setTimeout(() => {
			if (authData) {
				router.replace('/profile');
				return;
			}
			router.replace('/');
		}, 0);
	}, [authData, router]);

	useEffect(() => {
		setTimeout(() => SplashScreen.hideAsync(), 300);
	}, []);

	return (
		<SafeAreaProvider>
			<ThemeProvider value={DefaultTheme}>
				<Stack>
					<Stack.Screen name="(auth)" options={{ headerShown: false }} />
					<Stack.Screen name="(user)" options={{ headerShown: false }} />
				</Stack>
			</ThemeProvider>
		</SafeAreaProvider>
	);
}
