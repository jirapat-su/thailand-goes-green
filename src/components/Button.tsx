import { styled } from 'nativewind';
import { Pressable, PressableProps, Text } from 'react-native';

interface ButtonProps extends PressableProps {
	text: string;
	primary?: boolean;
}

const StyledPressable = styled(Pressable);
const StyledText = styled(Text);

export default function Button(props: ButtonProps) {
	const { primary = false, text = '' } = props;

	return (
		<StyledPressable
			{...props}
			className={`${primary ? 'bg-primary active:bg-primary-600' : 'bg-white active:bg-gray-100'} w-full rounded-full pb-4 pt-4 shadow `}
		>
			<StyledText className={`${primary ? 'text-primary-100' : 'text-primary'} text-center text-lg font-semibold`}>{text}</StyledText>
		</StyledPressable>
	);
}
