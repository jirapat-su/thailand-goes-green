import { Image } from 'expo-image';
import { View, ViewProps } from 'react-native';

interface AvatarProps extends ViewProps {
	url: string;
}

export const blurhash =
	'|rF?hV%2WCj[ayj[a|j[az_NaeWBj@ayfRayfQfQM{M|azj[azf6fQfQfQIpWXofj[ayj[j[fQayWCoeoeaya}j[ayfQa{oLj?j[WVj[ayayj[fQoff7azayj[ayj[j[ayofayayayj[fQj[ayayj[ayfjj[j[ayjuayj[';

export default function Avatar(props: AvatarProps) {
	return (
		<View className="bg-lightgray h-40 w-40 overflow-hidden rounded-full border-8 border-white">
			<Image source={props.url} placeholder={blurhash} contentFit="cover" transition={1000} className="h-full w-full flex-1" />
		</View>
	);
}
