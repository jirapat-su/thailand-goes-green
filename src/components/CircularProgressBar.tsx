import React from 'react';
import { View } from 'react-native';
import Svg, { Circle, Text } from 'react-native-svg';

interface CircularProgressBarProps {
	score: number;
}

const CircularProgressBar = ({ score }: CircularProgressBarProps) => {
	const convertScore = (input: number): number => {
		if (input <= 100) {
			return parseFloat(input.toFixed(2));
		}
		const val = input / 10;
		return convertScore(val);
	};

	const RADIUS = 100;
	const STROKE_WIDTH = 8;
	const progressPercentage = convertScore(score);
	const circumference = 2 * Math.PI * (RADIUS - STROKE_WIDTH / 2);
	const strokeDashoffset = circumference - (circumference * progressPercentage) / 100;

	return (
		<View>
			<Svg width={RADIUS * 2} height={RADIUS * 2}>
				<Circle cx={RADIUS} cy={RADIUS} r={RADIUS - STROKE_WIDTH / 2} stroke="#E8E8E8" strokeWidth={STROKE_WIDTH} />
				<Circle
					cx={RADIUS}
					cy={RADIUS}
					r={RADIUS - STROKE_WIDTH / 2}
					stroke="#5DB075"
					strokeWidth={STROKE_WIDTH}
					strokeDasharray={circumference}
					strokeDashoffset={strokeDashoffset}
					strokeLinecap="round"
					fill="white"
				/>
				<Text x={RADIUS} y={RADIUS - 40} textAnchor="middle" fontSize={20} fontWeight="bold" fill="#525252">
					Plant More
				</Text>
				<Text x={RADIUS} y={RADIUS} textAnchor="middle" fontSize={28} fontWeight="bold" fill="#5DB075">
					{Math.abs(score - 1000)}
				</Text>
				<Text x={RADIUS} y={RADIUS + 32} textAnchor="middle" fontSize={12} fontWeight="normal" fill="#BDBDBD">
					To make your zone get fresh
				</Text>
			</Svg>
		</View>
	);
};

export default CircularProgressBar;
