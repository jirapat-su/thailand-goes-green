import { styled } from 'nativewind';
import { useCallback, useState } from 'react';
import { Text, TextInput, TextInputProps, TouchableOpacity, View } from 'react-native';

interface InputProps extends TextInputProps {}

const StyledView = styled(View);
const StyledTextInput = styled(TextInput);
const StyledTouchableOpacity = styled(TouchableOpacity);
const StyledText = styled(Text);

export default function Input(props: InputProps) {
	const { secureTextEntry = false } = props;

	const [showPassword, setShowPassword] = useState(false);

	const toggleShowPassword = useCallback(() => {
		setShowPassword((prevShowPassword) => !prevShowPassword);
	}, [setShowPassword]);

	return (
		<StyledView className="flex w-full">
			<StyledView className="relative w-[inherit]">
				<StyledTextInput
					{...props}
					className="w-[inherit] rounded border border-gray-200 bg-gray-100 p-4 pr-10"
					secureTextEntry={secureTextEntry ? !showPassword : false}
					placeholderTextColor={'#cdcdcd'}
				/>
				{secureTextEntry && (
					<StyledTouchableOpacity className="absolute bottom-4 right-4" onPress={toggleShowPassword}>
						<StyledText className="text-primary">{showPassword ? 'Hide' : 'Show'}</StyledText>
					</StyledTouchableOpacity>
				)}
			</StyledView>
		</StyledView>
	);
}
