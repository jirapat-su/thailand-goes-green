import AsyncStorage from '@react-native-async-storage/async-storage';
import { combineReducers, configureStore } from '@reduxjs/toolkit';
import logger from 'redux-logger';
import { FLUSH, PAUSE, PERSIST, PURGE, REGISTER, REHYDRATE, persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';
import { authReducer } from './slices/authSlice';
import { plantReducer } from './slices/plantSlice';

const rootPersistConfig = {
	key: 'root',
	storage: AsyncStorage,
	blacklist: ['auth']
};

const authPersistConfig = {
	key: 'auth',
	storage: AsyncStorage
};

const rootReducer = combineReducers({
	auth: persistReducer(authPersistConfig, authReducer),
	plant: plantReducer
});

const rootPersistReducer = persistReducer(rootPersistConfig, rootReducer);

export const store = configureStore({
	reducer: rootPersistReducer,
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: {
				ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
			}
		}).concat([thunk, logger])
});
export const persistor = persistStore(store);
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
