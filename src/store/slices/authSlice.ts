import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface AuthState {
	username: string;
	token: string;
}

const initialState: { data: AuthState | null } = {
	data: null
};

const authSlice = createSlice({
	name: 'auth',
	initialState,
	reducers: {
		setAuthData: (state, action: PayloadAction<AuthState | null>) => {
			state.data = action.payload;
		}
	}
});

export const { setAuthData } = authSlice.actions;
export const authReducer = authSlice.reducer;
