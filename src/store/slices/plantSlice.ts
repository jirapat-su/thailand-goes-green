import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface PlantState {
	score: number;
}

const initialState: PlantState = { score: 0 };

const plantSlice = createSlice({
	name: 'plant',
	initialState,
	reducers: {
		setPlantScore: (state, action: PayloadAction<number>) => {
			state.score = action.payload;
		}
	}
});

export const { setPlantScore } = plantSlice.actions;
export const plantReducer = plantSlice.reducer;
